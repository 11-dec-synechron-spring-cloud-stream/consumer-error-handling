package com.classpath.consumererrorhandling.exception;

import lombok.extern.slf4j.Slf4j;
import org.springframework.integration.annotation.ServiceActivator;
import org.springframework.messaging.support.ErrorMessage;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class ExceptionHandler {

    @ServiceActivator(inputChannel = "topic/in/messages.consumergroup.errors")
    public void handleInvalidMessage(ErrorMessage errorMessage){
        log.error("Handling the error message :: {}", errorMessage.getOriginalMessage());
    }

    @ServiceActivator(inputChannel = "errorChannel")
    public void handleExceptions(ErrorMessage errorMessage){
        log.error(":::: Handling generic exceptions :::: , {}", errorMessage.getOriginalMessage());
        log.error("Handling the error message :: {}", errorMessage.getOriginalMessage());
    }
}